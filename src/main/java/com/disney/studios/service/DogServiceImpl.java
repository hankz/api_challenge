package com.disney.studios.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.disney.studios.entity.Dog;
import com.disney.studios.repository.DogDao;
import com.disney.studios.util.Constants;

@Service("dogService")
public class DogServiceImpl implements DogService {

	@Autowired
	private DogDao dogDao;

	public Iterable<String> getDogPictures() {

		Sort sort = new Sort(new Sort.Order(Sort.Direction.ASC, "breed"));
		List<String> list = new ArrayList<String>();
		List<Dog> dogs = dogDao.findAll(sort);
		Iterator<Dog> it = dogs.iterator();
		while (it.hasNext()) {
			list.add(it.next().getImageUrl());
		}
		return list;
	}

	public Iterable<String> getDogPicturesByBreed(String breed) {
		List<String> list = new ArrayList<String>();
		List<Dog> dogs = dogDao.findByBreed(breed);
		Iterator<Dog> it = dogs.iterator();
		while (it.hasNext()) {
			list.add(it.next().getImageUrl());
		}
		return list;
	}

	public String voteForDog(String imageUrl,boolean up) {
		try {
			int num = 0;
			if(up){
				num = 1;
			}else{
				num = -1;
			}
			Dog dog = dogDao.findByImageUrl(imageUrl);
			dog.setFavorited(dog.getFavorited()+num);
			
			dogDao.save(dog);

		} catch (Exception e) {
			// System.out.println(e);
			return Constants.ERROR;
		}

		return Constants.SUCCESS;

	}

	public Dog getDogDetailsByImageUrl(@PathVariable String imageUrl){
		return	dogDao.findByImageUrl(imageUrl);
	}

}
