package com.disney.studios.service;

import com.disney.studios.entity.Dog;

public interface DogService {
	

	public Iterable<String>  getDogPictures() ;
	

	public Iterable<String>  getDogPicturesByBreed(String breed); 
	
	public String voteForDog(String imageUrl,boolean up) ;
	

	public Dog getDogDetailsByImageUrl(String imageUrl) ;

}
