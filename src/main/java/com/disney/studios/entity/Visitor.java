package com.disney.studios.entity;

import java.util.ArrayList;
import java.util.List;

public class Visitor {
	private List<String> visited = new  ArrayList<String>();
	
	private String ipAddress;
	
	public Visitor(String ipAddress){
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public void addPiuctureVoted(String url){
		visited.add(url);
		
	}
	
	public boolean voted(String url){
		return this.visited.contains(url);
	}
	

}
