package com.disney.studios.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;

@Entity
public class Dog implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long Id;
	private static final long serialVersionUID = 1L;
	private String breed;
	private String imageUrl;
	private Long favorited;
	@Transient
	private boolean favoritedUp;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public boolean isFavoritedUp() {
		return favoritedUp;
	}

	public void setFavoritedUp(boolean favoritedUp) {
		this.favoritedUp = favoritedUp;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getFavorited() {
		return favorited;
	}

	public void setFavorited(Long favorited) {
		this.favorited = favorited;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		result = prime * result + ((breed == null) ? 0 : breed.hashCode());
		result = prime * result + ((favorited == null) ? 0 : favorited.hashCode());
		result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dog other = (Dog) obj;
		if (Id != other.Id)
			return false;
		if (breed == null) {
			if (other.breed != null)
				return false;
		} else if (!breed.equals(other.breed))
			return false;
		if (favorited == null) {
			if (other.favorited != null)
				return false;
		} else if (!favorited.equals(other.favorited))
			return false;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		return true;
	}

}
