package com.disney.studios.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.disney.studios.entity.Dog;
import com.disney.studios.entity.Visitor;
import com.disney.studios.service.DogService;
import com.disney.studios.util.Constants;

@Controller
@SessionAttributes("visitor")
public class DogController {

	@Autowired
	private DogService dogService;

	@ModelAttribute("visitor")
	public Visitor getVisitor(HttpServletRequest request) {
		String ip = request.getRemoteAddr();
		return new Visitor(request.getRemoteAddr());
	}

	// group by breed
	@RequestMapping(value = "/dogs/pictures", method = RequestMethod.GET)
	@ResponseBody
	public Iterable<String> getDogPictures() {

		return dogService.getDogPictures();

	}

	@RequestMapping(value = "/dogs/pictures/{breed}", method = RequestMethod.GET)
	@ResponseBody
	public Iterable<String> getDogPicturesByBreed(@PathVariable String breed) {

		return dogService.getDogPicturesByBreed(breed);

	}

	@RequestMapping(value = "/voteforDog", method = RequestMethod.POST)
	@ResponseBody

	public String voteForDog(@ModelAttribute("visitor") Visitor visitor, @RequestParam(value = "imageUrl", required = true) String imageUrl,
			@RequestParam(value = "up", required = true) String up) {
		String status = Constants.ERROR;
        if(! visitor.voted(imageUrl)){
        	status = dogService.voteForDog(imageUrl, Boolean.valueOf(up));
        }
        return status;

	}

	@RequestMapping(value = "/dog/{imageUrl}", method = RequestMethod.GET)
	@ResponseBody
	public Dog getDogDetailsByImageUrl(@PathVariable String imageUrl) {

		return dogService.getDogDetailsByImageUrl(imageUrl);

	}

}