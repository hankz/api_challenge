package com.disney.studios.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.disney.studios.entity.Dog;

@Transactional
@Repository
public interface DogDao extends CrudRepository<Dog, Long> {

	public List<Dog> findAll(Sort sort);

	public List<Dog> findByBreed(String breed);
	public Dog findByImageUrl(String imageUrl);


}
